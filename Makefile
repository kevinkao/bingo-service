
all: build
build:
	go install bingo

run: build
	./bin/bingo

deps:
	go get -u github.com/kevinkao/migrate/migrate
	go get -u github.com/kevinkao/seeder/seeder