package controllers

import (
	"time"
	"net/http"
	"database/sql"
	"github.com/gin-gonic/gin"
	"github.com/dgrijalva/jwt-go"

	"events"
	repo "repositories"
	"repositories/binding"
)

type CustomClaims struct {
	MemberId int64 `json:"memberId"`
	Account string `json:"account"`
	Name string `json:"name"`
	LevelId binding.NullInt64 `json:"levelId"`
	ParentId binding.NullInt64 `json:"parentId"`
	jwt.StandardClaims
}

type AuthPair struct {
	Account string `json:"account" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type AuthController struct {
	DB *sql.DB
	JwtSecret string
}

func (ac *AuthController) Auth (c *gin.Context) {
	var pair *AuthPair
	if err := c.ShouldBindJSON(&pair); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{ "code": CODE_INVALIDATE_INPUT, "message": err.Error() })
		return
	}

	eventBus := c.MustGet("eventBus").(*events.EventBus)

	hash := Hash {}
	memberRepo := &repo.MemberRepository{ ac.DB, eventBus }
	member := memberRepo.FindByAccount(pair.Account)

	if hash.Check(member.GetPassword(), pair.Password) {
		if member.Blocked > 0 {
			c.JSON(http.StatusOK, gin.H{
				"code": CODE_MEMBER_BLOCKED,
				"message": "You have been blocked!",
			})
			c.Abort()
			return
		}

		claims := new(CustomClaims)
		claims.MemberId = member.Id
		claims.Account = member.Account
		claims.Name = member.Name
		claims.LevelId = member.LevelId
		claims.ParentId = member.ParentId
		claims.StandardClaims = jwt.StandardClaims{
			IssuedAt: time.Now().Unix(),
			ExpiresAt: time.Now().Unix() + 60 * 30,
		}

		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
		tokenString, err := token.SignedString([]byte(ac.JwtSecret))
		if err != nil {
			panic(err)
		}

		c.JSON(http.StatusOK, gin.H{
			"code": CODE_SUCCESS,
			"message": "ok",
			"token": tokenString,
		})
	} else {
		c.JSON(http.StatusOK, gin.H{ "code": CODE_UNKNOWN_ERROR, "message": "error" })	
	}
}
