package controllers

import "golang.org/x/crypto/bcrypt"

type Hash struct {}

func (h *Hash) Make (secret string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(secret), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(hash), nil
}

func (h *Hash) Check (hashedSecret string, secret string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashedSecret), []byte(secret))
	if err != nil {
		return false
	} else {
		return true
	}
}