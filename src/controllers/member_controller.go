package controllers

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"fmt"

	// "events"
	repo "repositories"
)

type MemberRegisterForm struct {
	Account string 					`json:"account" binding:"required,min=6,max=25"`
	Name string 					`json:"name" binding:"required,min=6,max=100"`
	Password string 				`json:"password" binding:"required,min=6,max=25"`
	PasswordConfirmation string 	`json:"password_confirmation" binding:"required,min=6,max=25"`
}

type MemberController struct {}

func (mc *MemberController) ListByLevel (c *gin.Context) {
	defer func () {
		if r := recover(); r != nil {
			c.JSON(http.StatusOK, gin.H { "code": CODE_UNKNOWN_ERROR, "message": r })
			c.Abort()
		}
	}()

	page := c.DefaultQuery("page", "1")
	limit := c.DefaultQuery("limit", "20")

	pageInt, err := strconv.ParseInt(page, 10, 64)
	if err != nil {
		panic(err)
	}

	limitInt, err := strconv.ParseInt(limit, 10, 64)
	if err != nil {
		panic(err)
	}

	member, _ := c.MustGet("member").(*repo.Member)
	level, _ := c.MustGet("level").(*repo.SessionLevel)

	var members []repo.Member
	members = member.GetChildByLevel(level, pageInt, limitInt)

	c.JSON(http.StatusOK, gin.H{
		"code": CODE_SUCCESS,
		"data": members,
	})
}

func (mc * MemberController) SearchByAccount (c *gin.Context) {
	defer func () {
		if r := recover(); r != nil {
			fmt.Println(r)
			c.JSON(http.StatusOK, gin.H { "code": CODE_UNKNOWN_ERROR, "message": r })
			c.Abort()
		}
	}()

	page := c.DefaultQuery("page", "1")
	limit := c.DefaultQuery("limit", "20")
	account := c.Param("account")

	pageInt, err := strconv.ParseInt(page, 10, 64)
	if err != nil {
		panic(err)
	}

	limitInt, err := strconv.ParseInt(limit, 10, 64)
	if err != nil {
		panic(err)
	}

	member, _ := c.MustGet("member").(*repo.Member)
	level, _ := c.MustGet("level").(*repo.SessionLevel)

	members := member.SearchChildByAccount(account, level, pageInt, limitInt)

	c.JSON(http.StatusOK, gin.H {
		"code": CODE_SUCCESS,
		"data": members,
	})
}

func (mc *MemberController) FindById (c *gin.Context) {
	theMember := c.MustGet("theMember").(*repo.Member)
	c.JSON(http.StatusOK, gin.H{
		"code": CODE_SUCCESS,
		"data": theMember,
	})	
}

func (mc *MemberController) AddMember (c *gin.Context) {
	defer func () {
		if r := recover(); r != nil {
			fmt.Println(r)
			c.JSON(http.StatusOK, gin.H { "code": CODE_UNKNOWN_ERROR, "message": r })
			c.Abort()
		}
	}()

	var form MemberRegisterForm
	if err := c.ShouldBindJSON(&form); err != nil {
		panic(err)
	}

	if form.Password != form.PasswordConfirmation {
		panic("Password confirmation fail")
	}

	theMember := c.MustGet("theMember").(*repo.Member)
	levelRepo := c.MustGet("levelRepo").(*repo.LevelRepository)
	memberRepo := c.MustGet("memberRepo").(*repo.MemberRepository)

	childLevel := levelRepo.GetChildLevel(theMember.LevelSequence)
	if len(childLevel) == 0 {
		panic("Can not add child under this member.")
	}

	m := repo.Member {}
	m.SetLevelId(childLevel[0].Id)
	m.SetParentId(theMember.Id)
	m.Account = form.Account
	m.Name = form.Name
	hash := Hash {}
	hashed, _ := hash.Make(form.Password)
	m.SetPassword(hashed)
	m.SetMemberRepo(memberRepo)
	m.Store()
	
	c.JSON(http.StatusOK, gin.H {
		"code": CODE_SUCCESS,
		"data": m,
	})
}

func (mc *MemberController) GetLimitation (c *gin.Context) {
	theMember := c.MustGet("theMember").(*repo.Member)
	limitation := theMember.GetLimitation()
	c.JSON(http.StatusOK, gin.H{
		"code": CODE_SUCCESS,
		"data": limitation.All(),
	})
}

func (mc *MemberController) UpdateLimitation (c *gin.Context) {
	defer func () {
		if r := recover(); r != nil {
			fmt.Println(r)
			c.JSON(http.StatusOK, gin.H { "code": CODE_UNKNOWN_ERROR, "message": r })
			c.Abort()
		}
	}()
	collection := c.MustGet("collection").(*repo.LimitationCollection)
	member := c.MustGet("theMember").(*repo.Member)
	affected := member.UpdateLimitation(*collection)
	c.JSON(http.StatusOK, gin.H{
		"code": CODE_SUCCESS,
		"data": affected.All(),
	})
}

func (mc *MemberController) GetPremium (c *gin.Context) {
	theMember := c.MustGet("theMember").(*repo.Member)
	premium := theMember.GetPremium()
	c.JSON(http.StatusOK, gin.H{
		"code": CODE_SUCCESS,
		"data": premium.All(),
	})
}

func (mc *MemberController) UpdatePremium (c *gin.Context) {
	defer func () {
		if r := recover(); r != nil {
			fmt.Println(r)
			c.JSON(http.StatusOK, gin.H { "code": CODE_UNKNOWN_ERROR, "message": r })
			c.Abort()
		}
	}()
	collection := c.MustGet("collection").(*repo.PremiumCollection)
	member := c.MustGet("theMember").(*repo.Member)
	affected := member.UpdatePremium(*collection)
	c.JSON(http.StatusOK, gin.H{
		"code": CODE_SUCCESS,
		"data": affected.All(),
	})
}

func (mc *MemberController) GetProportion (c *gin.Context) {
	theMember := c.MustGet("theMember").(*repo.Member)
	premium := theMember.GetProportion()
	c.JSON(http.StatusOK, gin.H{
		"code": CODE_SUCCESS,
		"data": premium,
	})
}

func (mc *MemberController) UpdateProportion (c *gin.Context) {
	defer func () {
		if r := recover(); r != nil {
			fmt.Println(r)
			c.JSON(http.StatusOK, gin.H { "code": CODE_UNKNOWN_ERROR, "message": r })
			c.Abort()
		}
	}()
	collection := c.MustGet("collection").(*repo.ProportionCollection)
	member := c.MustGet("theMember").(*repo.Member)
	affected := member.UpdateProportion(*collection)
	c.JSON(http.StatusOK, gin.H{
		"code": CODE_SUCCESS,
		"data": affected.All(),
	})
}
