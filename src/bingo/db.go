package main

import (
	"fmt"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

type Dsn struct {
	Username, Password, Host, Port, Database, Charset string
}

func InitDB (keys ...string) *sql.DB {
	var selection string
	if len(keys) > 0 {
		selection = keys[0]
	} else {
		selection = "default"
	}

	key := dbConfig.GetString(selection)
	dsn := &Dsn {
		Username: dbConfig.GetString(key+".username"),
		Password: dbConfig.GetString(key+".password"),
		Host: dbConfig.GetString(key+".host"),
		Port: dbConfig.GetString(key+".port"),
		Database: dbConfig.GetString(key+".database"),
		Charset: dbConfig.GetString(key+".charset"),
	}

	var err error
	db, err = sql.Open("mysql", fmt.Sprintf(
			"%s:%s@tcp(%s:%s)/%s?parseTime=true&multiStatements=true&charset=%s",
			dsn.Username,
			dsn.Password,
			dsn.Host,
			dsn.Port,
			dsn.Database,
			dsn.Charset,
		))
	if err != nil {
		panic(err)
	}
	return db
}
