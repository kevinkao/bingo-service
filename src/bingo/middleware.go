package main

import (
	"fmt"
	"strings"
	"net/http"
	"strconv"
	"time"
	"github.com/gin-gonic/gin"
	"github.com/dgrijalva/jwt-go"

	"controllers"
	repo "repositories"
)

func EventBusInjector (c *gin.Context) {
	c.Set("eventBus", eventBus)
}

func AuthRequired (c *gin.Context) {
	authText := c.Request.Header["Authorization"]
	var bearer []string
	var tokenString string

	defer func () {
		if r := recover(); r != nil {
			c.JSON(http.StatusOK, gin.H{ "code": controllers.CODE_UNAUTHORIZED, "message": r })
			c.Abort()
		}
	}()

	if len(authText) > 0 {
		bearer = strings.Split(strings.Trim(authText[0], " "), " ")
		if len(bearer) < 2 {
			panic("Invalid token")
		}
		tokenString = bearer[1]
	}

	if len(tokenString) == 0 {
		panic("Invalid token")
	}
	
	token, err := jwt.ParseWithClaims(tokenString, &controllers.CustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(appConfig.GetString("jwt_token_secret")), nil
	})

	if claims, ok := token.Claims.(*controllers.CustomClaims); ok && token.Valid {
		c.Set("claims", claims)

		memberRepo := repo.MemberRepository { db, eventBus }
		levelRepo := repo.LevelRepository{ db, eventBus }
		m := memberRepo.FindByAccount(claims.Account)

		if m.Blocked > 0 {
			c.JSON(http.StatusOK, gin.H{
				"code": controllers.CODE_MEMBER_BLOCKED,
				"message": "You have been blocked!",
			})
			c.Abort()
			return
		}

		now := time.Now().Unix()
		if claims.ExpiresAt - now <= 300 {
			// TODO
		}

		c.Set("memberRepo", &memberRepo)
		c.Set("levelRepo", &levelRepo)
		c.Set("member", &m)
		c.Next()
	} else if ve, ok := err.(*jwt.ValidationError); ok {
		if ve.Errors&jwt.ValidationErrorMalformed != 0 {
			panic("Invalid token")
		} else if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
			panic("Token is expired")
		} else {
			panic(fmt.Sprintf("Couldn't handle this token: %s", err))
		}
		c.Abort()
	} else {
		panic(fmt.Sprintf("Couldn't handle this token: %s", err))
	}
}

func HighLevelRequired (levelRequire int64) gin.HandlerFunc {
	return func (c *gin.Context) {
		member := c.MustGet("member").(*repo.Member)

		defer func () {
			if r := recover(); r != nil {
				c.JSON(http.StatusOK, gin.H{ "code": controllers.CODE_ACCESS_DENIED, "message": r })
				c.Abort()
			}
		}()

		// 只限制特定層級的人使用
		// 數字越小代表層級越高, 所以超過限定的數字代表層級不夠
		if member.LevelSequence > levelRequire {
			panic("Permission denied")
		}

		c.Next()
	}
}

func CanAccessParamLevel (keyName string) gin.HandlerFunc {
	return func (c *gin.Context) {
		defer func () {
			if r := recover(); r != nil {
				c.JSON(http.StatusOK, gin.H{ "code": controllers.CODE_UNAUTHORIZED, "message": r })
				c.Abort()
			}
		}()

		levelId, err := strconv.ParseInt(c.Param(keyName), 10, 64)
		if err != nil {
			panic(err)
		}

		member := c.MustGet("member").(*repo.Member)
		levelRepo := c.MustGet("levelRepo").(*repo.LevelRepository)
		level := levelRepo.FindById(levelId)

		c.Set("level", &repo.SessionLevel {
			repo.Level {
				level.Id,
				level.Name,
				level.Sequence,
			},
			levelRepo,
		})

		if !member.CanAccessLevel(level) {
			panic("Access level denied")
		}
	}
}

func CanAccessParamMemberByAccount (keyName string) gin.HandlerFunc {
	return func (c *gin.Context) {
		defer func () {
			if r := recover(); r != nil {
				c.JSON(http.StatusOK, gin.H{
					"code": controllers.CODE_UNAUTHORIZED,
					"message": r,
				})
				c.Abort()
			}
		}()

		memberRepo := c.MustGet("memberRepo").(*repo.MemberRepository)
		member := c.MustGet("member").(*repo.Member)

		targetAccount := c.Param(keyName)
		targetMember := memberRepo.FindByAccount(targetAccount)

		var parents []repo.Member
		memberRepo.GetParents(targetMember.Id, &parents)
		if len(parents) == 0 {
			panic(fmt.Sprintf("Can not access member: %s", targetMember.Account))
		}
		isChild := false
		for _, parent := range parents  {
			if parent.Id == member.Id {
				isChild = true
			}
		}

		if !isChild {
			panic(fmt.Sprintf("Can not access member: %s", targetMember.Account))
		}
	}
}

func CanAccessParamMemberId (keyName string) gin.HandlerFunc {
	return func (c *gin.Context) {
		defer func () {
			if r := recover(); r != nil {
				c.JSON(http.StatusOK, gin.H{
					"code": controllers.CODE_UNAUTHORIZED,
					"message": r,
				})
				c.Abort()
			}
		}()

		memberRepo := c.MustGet("memberRepo").(*repo.MemberRepository)
		member := c.MustGet("member").(*repo.Member)

		theId, err := strconv.ParseInt(c.Param(keyName), 10, 64)
		if err != nil {
			panic(err)
		}

		theMember := memberRepo.FindById(theId)

		isParent := false
		parents := theMember.GetParents()
		for _, parent := range parents {
			if parent.Id == member.Id {
				isParent = true
				break
			}
		}
		
		if !isParent {
			panic("Access denied")
		}

		c.Set("theMember", &theMember)
	}
}

func RangeInParentLimitation(c *gin.Context) {
	defer func () {
		if r := recover(); r != nil {
			c.JSON(http.StatusOK, gin.H{
				"code": controllers.CODE_INVALID_PARAMETERS,
				"message": r,
			})
			c.Abort()
		}
	}()

	var collection repo.LimitationCollection
	if err := c.ShouldBindJSON(&collection); err != nil {
		panic(err)
	}
	c.Set("collection", &collection)

	theMember := c.MustGet("theMember").(*repo.Member)
	parent, exists := theMember.GetParent()
	if !exists {
		// 沒有父會員, 不用檢查了
		c.Next()
		return
	}

	for _, limitation := range collection.Slice {
		// 取得玩法的限紅
		pl := parent.GetBetTypeLimitation(limitation.BetTypeId)
		if limitation.MinAmount < pl.MinAmount {
			panic(fmt.Sprintf("Invalid parameter minAmount: %g is smaller than %g from parent's limitation", limitation.MinAmount, pl.MinAmount))
		}

		// 取得父會員底下的所有限紅最大值加總並計算是否會超出父會員的最大值
		childSumMaxAmount := parent.GetChildLimitationMaxAmount(limitation.BetTypeId)
		theMemberLimitation := theMember.GetBetTypeLimitation(limitation.BetTypeId)
		if limitation.MaxAmount - theMemberLimitation.MaxAmount > pl.MaxAmount - childSumMaxAmount {
			panic(fmt.Sprintf("Invalid parameter maxAmount: %g will over than %g from parent's total max amount", limitation.MaxAmount, pl.MaxAmount))
		}
	}
}

func RangeInParentPremium(c *gin.Context) {
	defer func () {
		if r := recover(); r != nil {
			c.JSON(http.StatusOK, gin.H{
				"code": controllers.CODE_INVALID_PARAMETERS,
				"message": r,
			})
			c.Abort()
		}
	}()

	var collection repo.PremiumCollection
	if err := c.ShouldBindJSON(&collection); err != nil {
		panic(err)
	}
	c.Set("collection", &collection)

	theMember := c.MustGet("theMember").(*repo.Member)
	parent, exists := theMember.GetParent()
	if !exists {
		// 沒有父會員, 不用檢查了
		c.Next()
		return
	}

	for _, p := range collection.Slice {
		parentPremium := parent.GetBetTypePremium(p.BetTypeId)

		if p.Amount > parentPremium.Amount {
			fmt.Println(p.Amount, parentPremium.Amount)
			panic(fmt.Sprintf("Invalid parameter amount: %g is more than parent's amount: %g", p.Amount, parentPremium.Amount))
		}
	}
}

func SmallerThanParentsProportion (c *gin.Context) {
	defer func () {
		if r := recover(); r != nil {
			c.JSON(http.StatusOK, gin.H{
				"code": controllers.CODE_INVALID_PARAMETERS,
				"message": r,
			})
			c.Abort()
		}
	}()

	var collection repo.ProportionCollection
	if err := c.ShouldBindJSON(&collection); err != nil {
		panic(err)
	}
	c.Set("collection", &collection)

	theMember := c.MustGet("theMember").(*repo.Member)
	parent, exists := theMember.GetParent()
	if !exists {
		// 沒有父會員, 不用檢查了
		c.Next()
		return
	}

	for _, p := range collection.Slice {
		parentPropotion := parent.GetBetTypeProportion(p.BetTypeId)
		if p.Amount > parentPropotion.Amount {
			panic(fmt.Sprintf("Invalid parameter amount: %g is more than parent's amount: %g", p.Amount, parentPropotion.Amount))
		}
	}
}

func NotAllowUpdateSelf (keyName string) gin.HandlerFunc {
	return func (c *gin.Context) {
		defer func () {
			if r := recover(); r != nil {
				c.JSON(http.StatusOK, gin.H{
					"code": controllers.CODE_ILLEGAL_ACTION,
					"message": r,
				})
				c.Abort()
			}
		}()
		member := c.MustGet("member").(*repo.Member)
		theMember := c.MustGet("theMember").(*repo.Member)
		if member.Id == theMember.Id {
			panic("Can not update self!")
		}
	}
}