package main

import(
	"events"
	"fmt"
	"database/sql"
	"strings"

	repo "repositories"
)

type ProportionNode struct {
	Id int64
	ParentId int64
	Amount float32
}

func MakeProportionTree (tx *sql.Tx, betTypeId int64, baseChildren []int64) ProportionTree {
	pt := ProportionTree {
		tx,
		betTypeId,
		make(map[int64]ProportionNode),
		make(map[int64]ProportionNode),
	}
	stmt, err := tx.Prepare(`
		SELECT
			m.id,
			m.parent_id,
			mp.amount
		FROM member m
		LEFT JOIN member_proportion mp
			ON mp.member_id = m.id
		WHERE mp.bettype_id = ? AND m.id IN (?` + strings.Repeat(",?", len(baseChildren)-1) + `)`)

	if err != nil { panic(err) }
	defer stmt.Close()

	var args []interface{}
	args = append(args, betTypeId)
	for _, id := range baseChildren {
		args = append(args, id)
	}
	rows, err := stmt.Query(args...)
	defer rows.Close()
	if err != nil { panic(err) }
	
	for rows.Next() {
		p := ProportionNode{}
		err := rows.Scan(&p.Id, &p.ParentId, &p.Amount)
		if err != nil { panic(err) }
		pt.BaseChildren[p.Id] = p
	}
	return pt
}

type ProportionTree struct {
	Tx *sql.Tx
	BetTypeId int64
	Parents map[int64]ProportionNode
	BaseChildren map[int64]ProportionNode
}

func (pt *ProportionTree) EachChildren(callback func(idx int64, node ProportionNode)) {
	for idx, p := range pt.BaseChildren {
		callback(idx, p)
	}
}

func (pt *ProportionTree) CalculateParentsPropotion (parentId int64, childAmount float32, currentAmount float32) float32 {
	parent := pt.GetParent(parentId)
	currentAmount = currentAmount + parent.Amount - childAmount
	if parent.ParentId > 0 {
		return pt.CalculateParentsPropotion(parent.ParentId, parent.Amount, currentAmount)
	}
	return currentAmount
}

func (pt *ProportionTree) GetParent (idx int64) ProportionNode {
	p, ok := pt.Parents[idx]
	if !ok {
		row := pt.Tx.QueryRow(`
			SELECT
				m.id,
				m.parent_id,
				mp.amount
			FROM member m
			LEFT JOIN member_proportion mp
				ON mp.member_id = m.id
			WHERE mp.bettype_id = ? AND m.id = ?`, pt.BetTypeId, idx)

		var id int64
		var parentId sql.NullInt64
		var amount float32
		err := row.Scan(&id, &parentId, &amount)
		if err != nil { panic(err) }

		pt.Parents[id] = ProportionNode {
			Id: id,
			ParentId: func() int64 {
				if parentId.Valid {
					return parentId.Int64
				}
				return 0
			}(),
			Amount: amount,
		}
		return pt.Parents[id]
	}
	return p
}

func setupListeners (e *events.EventBus) {

	e.Subscribe(events.EVENT_LIMITATION_UPDATED, func(args ...interface{}) {
		memberId := args[0].(int64)
		affected := args[1].(repo.Collection)

		_, err := repo.WithTransaction (db, func(tx *sql.Tx) (interface{}, error) {

			for _, item := range affected.All() {
				limitation := item.(repo.Limitation)
				fmt.Println("each affected: ", limitation)

				eachMember(tx, memberId, func(parentId int64, children []int64, levelSequence int64) {
					fmt.Println("parent:", parentId)
					fmt.Println(" > ", children)

					// Lock first
					locks := int64ToInterfaceSlice(children)
					_, err := tx.Exec(`SELECT member_id FROM member_limitation WHERE member_id IN (?` + strings.Repeat(",?", len(children)-1) + `) FOR UPDATE`, locks...)
					if err != nil { panic(err) }

					parentRow := tx.QueryRow(`SELECT min_amount, max_amount FROM member_limitation WHERE member_id = ? AND bettype_id = ?`, parentId, limitation.BetTypeId)
					var parentMinAmount float32
					var parentMaxAmount float32
					err = parentRow.Scan(&parentMinAmount, &parentMaxAmount)
					if err != nil { panic(err) }

					var args1 []interface{}
					args1 = append(args1, limitation.MinAmount)
					args1 = append(args1, limitation.MinAmount)
					args1 = append(args1, limitation.BetTypeId)
					for _, id := range children {
						args1 = append(args1, id)
					}

					stmt1, err := tx.Prepare(`UPDATE member_limitation SET min_amount = ? WHERE min_amount < ? AND bettype_id = ? AND member_id IN (?` + strings.Repeat(",?", len(children)-1) + `)`)
					if err != nil { panic(err) }
					_, err = stmt1.Exec(args1...)
					if err != nil { panic(err) }
					stmt1.Close()

					var args2 []interface{}
					args2 = append(args2, limitation.BetTypeId)
					for _, id := range children {
						args2 = append(args2, id)
					}
					childRow := tx.QueryRow(`SELECT SUM(max_amount) FROM member_limitation WHERE bettype_id = ? AND member_id IN (?` + strings.Repeat(",?", len(children)-1) + `)`, args2...)
					var childSummaryAmount float32
					err = childRow.Scan(&childSummaryAmount)
					if err != nil { panic(err) }

					if childSummaryAmount > parentMaxAmount {
						var args3 []interface{}
						args3 = append(args3, parentMaxAmount / childSummaryAmount)
						args3 = append(args3, limitation.BetTypeId)
						for _, id := range children {
							args3 = append(args3, id)
						}
						_, err := tx.Exec(`UPDATE member_limitation SET max_amount = FLOOR(max_amount * ?) WHERE bettype_id = ? AND member_id IN (?` + strings.Repeat(",?", len(children)-1) + `)`, args3...)
						if err != nil { panic(err) }
					}
				})
			}

			return nil, nil
		})
		if err != nil { panic(err) }
	})

	e.Subscribe(events.EVENT_PREMIUM_UPDATED, func(args ...interface{}) {
		memberId := args[0].(int64)
		affected := args[1].(repo.Collection)

		_, err := repo.WithTransaction (db, func(tx *sql.Tx) (interface{}, error) {
			id := []int64{ memberId }

			for _, item := range affected.All() {
				premium := item.(repo.Premium)
				fmt.Println("each affected: ", premium)

				eachMemberLevelByLevel(tx, id, func(children []int64) {
					// Lock first
					locks := int64ToInterfaceSlice(children)
					_, err := tx.Exec(`SELECT member_id FROM member_premium WHERE member_id IN (?` + strings.Repeat(",?", len(children)-1) + `) FOR UPDATE`, locks...)
					if err != nil { panic(err) }

					stmt, err := tx.Prepare(
						`UPDATE member_premium SET amount=?
						WHERE bettype_id = ? AND amount > ? AND member_id IN (?` + strings.Repeat(",?", len(children)-1) + `)`)
					if err != nil { panic(err) }

					defer stmt.Close()

					var args []interface{}
					args = append(args, premium.Amount)
					args = append(args, premium.BetTypeId)
					args = append(args, premium.Amount)
					for _, id := range children {
						args = append(args, id)
					}
					_, err = stmt.Exec(args...)
					if err != nil { panic(err) }
				})
			}

			return nil, nil
		})
		if err != nil { panic(err) }
	})

	e.Subscribe(events.EVENT_PROPORTION_UPDATED, func(args ...interface{}) {
		memberId := args[0].(int64)
		affected := args[1].(repo.Collection)

		_, err := repo.WithTransaction (db, func(tx *sql.Tx) (interface{}, error) {
			for _, item := range affected.All() {
				proportion := item.(repo.Proportion)
				fmt.Println("each affected: ", proportion)
				var agentSlice []int64

				eachMemberExcludeLevel(tx, memberId, repo.LEVEL_MEMBER_SEQUENCE, func(parentId int64, children []int64, levelSequence int64) {
					// 只需要計算到代理層, 會員層沒有佔成不用作處理
					fmt.Println("parent:", parentId)
					fmt.Println("child level:", levelSequence)
					fmt.Println("child id > ", children)

					parentRow := tx.QueryRow(`SELECT amount FROM member_proportion WHERE member_id = ? AND bettype_id = ?`, parentId, proportion.BetTypeId)
					var parentAmount float32
					err := parentRow.Scan(&parentAmount)
					if err != nil { panic(err) }

					stmt, err := tx.Prepare(
						`UPDATE member_proportion SET amount=?
						WHERE bettype_id = ? AND amount > ? AND member_id IN (?` + strings.Repeat(",?", len(children)-1) + `)`)
					if err != nil { panic(err) }

					defer stmt.Close()
					var args []interface{}
					args = append(args, parentAmount)
					args = append(args, proportion.BetTypeId)
					args = append(args, parentAmount)
					for _, id := range children {
						args = append(args, id)
					}
					_, err = stmt.Exec(args...)
					if err != nil { panic(err) }

					if levelSequence == repo.LEVLE_AGENT_SEQUENCE {
						for _, id := range children {
							agentSlice = append(agentSlice, id)
						}
					}
				})

				// if len(agentSlice) > 0 {
				// 	pTree := MakeProportionTree(tx, proportion.BetTypeId, agentSlice)
				// 	pTree.EachChildren(func (id int64, node ProportionNode) {
				// 		amount := pTree.CalculateParentsPropotion(
				// 			node.ParentId,
				// 			node.Amount,
				// 			0,
				// 		)
				// 		fmt.Println("xxxxxx", amount)
				// 	})
				// }
			}

			return nil, nil
		})

		if err != nil { panic(err) }
	})

}

func int64ToInterfaceSlice(slice []int64) []interface{} {
	args := make([]interface{}, len(slice))
	for i, v := range slice {
		args[i] = v
	}
	return args
}

func eachMemberExcludeLevel (tx *sql.Tx, id int64, exclude int64, handler func(parentId int64, children []int64, levelSequence int64)) {
	rows, err := tx.Query(
		`SELECT member.id, level.sequence
		FROM member
			LEFT JOIN level ON level.id = member.level_id
		WHERE parent_id = ? AND level.sequence NOT IN (?)`, id, exclude)
	if err != nil { panic(err) }
	defer rows.Close()

	var children []int64
	var sequence int64
	for rows.Next() {
		var id int64
		err := rows.Scan(&id, &sequence)
		if err != nil { panic(err) }
		children = append(children, id)
	}
	if len(children) == 0 {
		return
	}
	handler(id, children, sequence)
	for _, childId := range children {
		eachMemberExcludeLevel(tx, childId, exclude, handler)
	}
}

func eachMember (tx *sql.Tx, id int64, handler func(parentId int64, children []int64, levelSequence int64)) {
	rows, err := tx.Query(`SELECT member.id, level.sequence FROM member LEFT JOIN level ON level.id = member.level_id WHERE parent_id = ?`, id)
	if err != nil { panic(err) }
	defer rows.Close()

	var children []int64
	var sequence int64
	for rows.Next() {
		var id int64
		err := rows.Scan(&id, &sequence)
		if err != nil { panic(err) }
		children = append(children, id)
	}
	if len(children) == 0 {
		return
	}
	handler(id, children, sequence)
	for _, childId := range children {
		eachMember(tx, childId, handler)
	}
}

func eachMemberLevelByLevel (tx *sql.Tx, id []int64, handler func(members []int64)) {
	args := int64ToInterfaceSlice(id)

	rows, err := tx.Query(`SELECT id FROM member WHERE parent_id IN (?` + strings.Repeat(",?", len(id)-1) + `)`, args...)
	if err != nil { panic(err) }
	defer rows.Close()

	var matches []int64
	for rows.Next() {
		var id int64
		err := rows.Scan(&id)
		if err != nil { panic(err) }
		matches = append(matches, id)
	}

	fmt.Println("child length: ", len(matches))
	if len(matches) == 0 {
		fmt.Println("no more child, end search.")
		return
	}
	handler(matches)
	eachMemberLevelByLevel(tx, matches, handler)
}
