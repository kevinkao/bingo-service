package main

import (
	"os"
	"path/filepath"
	"github.com/spf13/viper"
)

func InitConfig () (*viper.Viper, *viper.Viper) {
	currentPath, err := filepath.Abs(filepath.Dir(os.Args[0]))
	configPath := currentPath + "/../config"
	HandleError(err)

	appConfig = viper.New()
	appConfig.AddConfigPath(configPath)
	appConfig.SetConfigType("json")
	appConfig.SetConfigName("app")
	appConfig.ReadInConfig()

	dbConfig = viper.New()
	dbConfig.AddConfigPath(configPath)
	dbConfig.SetConfigType("json")
	dbConfig.SetConfigName("database")
	dbConfig.ReadInConfig()

	if FileExists(configPath + "/database.env.json") {
		dbConfig.SetConfigName("database.env")
		dbConfig.MergeInConfig()
	}

	return appConfig ,dbConfig
}