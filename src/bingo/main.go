package main

import (
	"os"
	"log"
	"github.com/gin-gonic/gin"
	"database/sql"
	"github.com/spf13/viper"
	"path/filepath"

	"events"
	"controllers"
	repo "repositories"
)

var logger *log.Logger
var appConfig *viper.Viper
var dbConfig *viper.Viper
var db *sql.DB

var authController *controllers.AuthController
var memberController *controllers.MemberController

var eventBus *events.EventBus

func main () {
	// 初始化logger
	currentPath, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}
	logPath := currentPath + "/../log/app.log"
	f, err := os.OpenFile(logPath, os.O_RDWR | os.O_CREATE | os.O_APPEND, 0666)
    if err != nil {
        log.Fatalf("error opening file: %v", err)
    }
    defer f.Close()
    logger = log.New(f, "bingo ", log.Lshortfile|log.LstdFlags)

    // 初始化config
	appConfig, dbConfig = InitConfig()

	eventBus = events.InitEventBus()
	setupListeners(eventBus)

	// 初始化db
	db = InitDB()
	defer db.Close()

	authController = &controllers.AuthController { db, appConfig.GetString("jwt_token_secret") }
	memberController = &controllers.MemberController {}

	r := gin.Default()

	r.Use(EventBusInjector)

	admin := r.Group("/admin")
	admin.Use(AuthRequired, HighLevelRequired(repo.LEVLE_AGENT_SEQUENCE))
	{
		level := admin.Group("/level")
		level.GET("/:level_id/member", CanAccessParamLevel("level_id"), memberController.ListByLevel)
		level.GET("/:level_id/member/search/:account", CanAccessParamLevel("level_id"), memberController.SearchByAccount)
		
		member := admin.Group("/member")
		member.Use(CanAccessParamMemberId("id"))
		member.GET("/:id", memberController.FindById)
		member.POST("/:id/child", memberController.AddMember)
		member.GET("/:id/limitation", memberController.GetLimitation)
		member.PUT("/:id/limitation", NotAllowUpdateSelf("id"), RangeInParentLimitation, memberController.UpdateLimitation)
		member.GET("/:id/premium", memberController.GetPremium)
		member.PUT("/:id/premium", NotAllowUpdateSelf("id"), RangeInParentPremium, memberController.UpdatePremium)
		member.GET("/:id/proportion", memberController.GetProportion)
		member.PUT("/:id/proportion", NotAllowUpdateSelf("id"), SmallerThanParentsProportion, memberController.UpdateProportion)
	}

	r.POST("/auth", authController.Auth)

	r.Run(":7272")
}
