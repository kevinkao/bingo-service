package main

import (
	"os"
)

func HandleError (err error) {
	if err != nil {
		panic(err)
	}
}

func FileExists (path string) bool {
	if _, err := os.Stat(path); err == nil {
		return true
	} else if os.IsNotExist(err) {
		return false
	}
	return false
}