package events

const (
	EVENT_LIMITATION_UPDATED = "limitation.updated"
	EVENT_PREMIUM_UPDATED = "premium.updated"
	EVENT_PROPORTION_UPDATED = "proportion.updated"
)