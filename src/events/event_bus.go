package events

import (
	// "fmt"
	"reflect"
)

type EventPack struct {
	Name string
	Args []interface{}
}

type EventBus struct {
	subscriber map[string][]func(args ...interface{})
	eventChan chan EventPack
}

func (eb *EventBus) Emit (evnetPack EventPack) {
	eb.eventChan <- evnetPack
}

func (eb *EventBus) Subscribe (eventName string, handler func(args ...interface{})) {
	eb.subscriber[eventName] = append(eb.subscriber[eventName], handler)
}

func (eb *EventBus) Unsubscribe (eventName string, cancelHander func(args ...interface{})) {
	var targetIdx int
	if handlers, ok := eb.subscriber[eventName]; ok {
		for i, handler := range handlers {
			if reflect.ValueOf(handler).Pointer() == reflect.ValueOf(cancelHander).Pointer() {
				targetIdx = i
				break;
			}
		}
		eb.subscriber[eventName] = append(eb.subscriber[eventName][:targetIdx], eb.subscriber[eventName][targetIdx+1:]...)
	}
}

func (eb *EventBus) Start () {
	for event := range eb.eventChan {
		if handlers, ok := eb.subscriber[event.Name]; ok {
			for _, handler := range handlers {
				go handler(event.Args...)
			}
		}
	}
}

func InitEventBus () *EventBus {
	eventBus := &EventBus {
		make(map[string][]func(args ...interface{})),
		make(chan EventPack),
	}

	go eventBus.Start()

	return eventBus
}

