package repositories

import (
	"database/sql"

	"events"
)

type LevelRepository struct {
	DB *sql.DB
	EventBus *events.EventBus
}

func (lr *LevelRepository) GetAllLevels () []Level {
	rows, err := lr.DB.Query(`SELECT id, name, sequence FROM level ORDER BY sequence`)
	if err != nil {
		panic(err)
	}

	var levels []Level
	for rows.Next() {
		var level Level
		err := rows.Scan(&level.Id, &level.Name, &level.Sequence)
		if err != nil {
			panic(err)
		}
		levels = append(levels, level)
	}

	return levels
}

func (lr *LevelRepository) FindById (levelId int64) Level {
	row := lr.DB.QueryRow(`SELECT id, name, sequence FROM level WHERE id = ?`, levelId)
	var level Level
	err := row.Scan(&level.Id, &level.Name, &level.Sequence)
	if err != nil {
		panic(err)
	}
	return level
}

func (lr *LevelRepository) GetChildLevel (sequence int64) []Level {
	rows, err := lr.DB.Query(`SELECT id, name, sequence FROM level WHERE sequence > ? ORDER BY sequence`, sequence)
	if err != nil {
		panic(err)
	}

	var levels []Level
	for rows.Next() {
		var level Level
		err := rows.Scan(&level.Id, &level.Name, &level.Sequence)
		if err != nil {
			panic(err)
		}
		levels = append(levels, level)
	}

	return levels
}