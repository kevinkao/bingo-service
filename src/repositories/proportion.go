package repositories

type Proportion struct {
	BetTypeId int64 			`json:"betTypeId" binding:"required"`
	BetTypeName string 			`json:"betTypeName"`
	BetTypeDisplayName string 	`json:"betTypeDisplayName"`
	Amount float32 				`json:"amount" binding:"required,min=1,max=100"`
}

type ProportionCollection struct {
	Slice []Proportion 	`json:"data" binding:"required,dive"`
}