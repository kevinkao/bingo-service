package repositories

import (
	"database/sql"
	"strings"
	"time"
	"repositories/binding"
	"fmt"

	"events"
)

type MemberRepository struct {
	DB *sql.DB
	EventBus *events.EventBus
}

func (mr *MemberRepository) scanIntoMemberStruct (rows *sql.Rows) []Member {
	var members []Member
	for rows.Next() {
		member := Member {}
		member.SetMemberRepo(mr)
		err := rows.Scan(
			&member.Id,
			&member.LevelId,
			&member.ParentId,
			&member.Name,
			&member.Account,
			&member.LevelName,
			&member.LevelSequence,
			&member.ParentName,
			&member.Blocked,
		)
		if err != nil {
			panic(err)
		}
		members = append(members, member)
	}

	if len(members) == 0 {
		return make([]Member, 0)
	}
	return members
}

func (mr *MemberRepository) FindById (id int64) Member {
	stmt, err := mr.DB.Prepare(
		`SELECT
			m.id, m.level_id, m.parent_id, m.name, m.account, level.name, level.sequence, pm.name AS parent_name, m.blocked
			FROM member m
			LEFT JOIN member pm ON pm.id = m.parent_id
			LEFT JOIN level ON level.id = m.level_id
			WHERE m.id = ?`)

	if err != nil {
		panic(err)
	}

	row := stmt.QueryRow(id)
	defer stmt.Close()

	member := Member {}
	member.SetMemberRepo(mr)
	if err := row.Scan(
		&member.Id,
		&member.LevelId,
		&member.ParentId,
		&member.Name,
		&member.Account,
		&member.LevelName,
		&member.LevelSequence,
		&member.ParentName,
		&member.Blocked,
	); err != nil {
		panic(err)
	}
	return member
}

func (mr *MemberRepository) FindByAccount (account string) Member {
	stmt, err := mr.DB.Prepare(
		`SELECT
			m.id, m.level_id, m.parent_id, m.name, m.account, level.name, level.sequence, pm.name AS parent_name, m.blocked, m.password
			FROM member m
			LEFT JOIN member pm ON pm.id = m.parent_id
			LEFT JOIN level ON level.id = m.level_id
			WHERE m.account = ?`)

	if err != nil {
		panic(err)
	}

	row := stmt.QueryRow(account)
	defer stmt.Close()

	member := Member {}
	member.SetMemberRepo(mr)
	if err := row.Scan(
		&member.Id,
		&member.LevelId,
		&member.ParentId,
		&member.Name,
		&member.Account,
		&member.LevelName,
		&member.LevelSequence,
		&member.ParentName,
		&member.Blocked,
		&member.password,
	); err != nil {
		panic(err)
	}

	return member
}

func (mr *MemberRepository) GetChild (memberId int64, page int64, limit int64) []Member {
	rows, err := mr.DB.Query(
		`SELECT
			m.id,
			m.level_id,
			m.parent_id,
			m.name,
			m.account,
			level.name,
			level.sequence,
			pm.name AS parent_name,
			m.blocked
		FROM member m
		LEFT JOIN member pm ON pm.id = m.parent_id
		LEFT JOIN level ON level.id = m.level_id
		WHERE m.parent_id = ? LIMIT ? OFFSET ?`, memberId, limit, (page - 1) * limit,
	)
	if err != nil {
		panic(err)
	}

	members := mr.scanIntoMemberStruct(rows)
	return members
}

/**
* 依照給定的會員id陣列依樹狀分層往下搜索，直到指定層級停止並回傳指定層級的會員id
* (如果要搜索的層級只是指定會員的下一層, 那就不用呼叫這個函式來逐層搜索, 直接返回指定會員的就好了)
*/
func (mr *MemberRepository) SearchMembersLevelByLevel (mids []int64, targetLevelId int64) []int64 {
	args := make([]interface{}, len(mids))
	for i, id := range mids {
		args[i] = id
	}

	stmt, err := mr.DB.Prepare(`SELECT id, level_id FROM member WHERE parent_id IN (?` + strings.Repeat(",?", len(mids)-1) + `)`)
	if err != nil {
		panic(err)
	}

	defer func () {
		stmt.Close()
	}()

	rows, err := stmt.Query(args...)
	if err != nil {
		panic(err)
	}

	var result []int64
	var queryId []int64
	var currentLevelId int64
	for rows.Next() {
		var id int64
		var levelId binding.NullInt64
		err := rows.Scan(&id, &levelId)
		if err != nil {
			panic(err)
		}
		currentLevelId = levelId.Int64
		queryId = append(queryId, id)
	}

	if currentLevelId == targetLevelId {
		result = append(result, queryId...)
	} else {
		result = append(result, mr.SearchMembersLevelByLevel(queryId, targetLevelId)...)
	}
	
	return result
}

func (mr *MemberRepository) ListWithinParents (parents []int64, page int64, limit int64) []Member {
	stmt, err := mr.DB.Prepare(
		`SELECT
			m.id,
			m.level_id,
			m.parent_id,
			m.name,
			m.account,
			level.name,
			level.sequence,
			pm.name AS parent_name,
			m.blocked
		FROM member m
		LEFT JOIN member pm ON pm.id = m.parent_id
		LEFT JOIN level ON level.id = m.level_id
		WHERE m.parent_id IN (?` + strings.Repeat(",?", len(parents)-1) + `) LIMIT ? OFFSET ?`)
	if err != nil {
		panic(err)
	}

	defer stmt.Close()

	var args []interface{}
	for _, id := range parents {
		args = append(args, id)
	}
	args = append(args, limit, (page - 1) * limit)

	rows, err := stmt.Query(args...)
	if err != nil {
		panic(err)
	}

	members := mr.scanIntoMemberStruct(rows)
	if len(members) == 0 {
		return make([]Member, 0)
	}
	return members
}

/**
* 在給定的父會員id之內模糊搜索賬號
*/
func (mr *MemberRepository) SearchAccountWithinParents (account string, parents []int64, page int64, limit int64) []Member {
	stmt, err := mr.DB.Prepare(
		`SELECT
			m.id,
			m.level_id,
			m.parent_id,
			m.name,
			m.account,
			level.name,
			level.sequence,
			pm.name AS parent_name,
			m.blocked
		FROM member m
		LEFT JOIN member pm ON pm.id = m.parent_id
		LEFT JOIN level ON level.id = m.level_id
		WHERE m.account LIKE ? AND m.parent_id IN (?` + strings.Repeat(",?", len(parents)-1) + `) LIMIT ? OFFSET ?`)
	if err != nil {
		panic(err)
	}

	defer stmt.Close()

	var args []interface{}
	args = append(args, account+`%`)
	for _, id := range parents {
		args = append(args, id)
	}
	args = append(args, limit, (page - 1) * limit)

	rows, err := stmt.Query(args...)
	if err != nil {
		panic(err)
	}

	members := mr.scanIntoMemberStruct(rows)
	if len(members) == 0 {
		return make([]Member, 0)
	}
	return members
}

/**
* 由指定的會員id一路網上搜尋出所有的父會員
*/
func (mr *MemberRepository) GetParents (childId int64, parents *[]Member) {
	row := mr.DB.QueryRow(
		`SELECT
			m.id,
			m.level_id,
			m.parent_id,
			m.name,
			m.account,
			level.name,
			level.sequence,
			pm.name AS parent_name,
			m.blocked
		FROM member m
		LEFT JOIN member pm ON pm.id = m.parent_id
		LEFT JOIN level ON level.id = m.level_id
		WHERE m.id = ?`,
		childId,
	)

	member := Member {}
	member.SetMemberRepo(mr)
	err := row.Scan(
		&member.Id,
		&member.LevelId,
		&member.ParentId,
		&member.Name,
		&member.Account,
		&member.LevelName,
		&member.LevelSequence,
		&member.ParentName,
		&member.Blocked,
	)
	if err != nil {
		fmt.Println(err)
		return
	}

	*parents = append(*parents, member)
	if !member.ParentId.Valid {
		// 往上還有父會員
		return
	}
	mr.GetParents(member.ParentId.Int64, parents)
}

func (mr *MemberRepository) AddMember(levelId int64, parentId int64, name string, account string, password string) (int64, error) {
	ret, err := WithTransaction (mr.DB, func(tx *sql.Tx) (interface{}, error) {
		result, err := tx.Exec(
			`INSERT INTO member
				(level_id, parent_id, name, account, password, created_at, updated_at)
			VALUES (?, ?, ?, ?, ?, ?, ?)`,
			levelId, parentId, name, account, password, time.Now(), time.Now())

		if err != nil { panic(err) }

		newMemberId, err := result.LastInsertId()

		if err != nil { panic(err) }
		fmt.Println("Member created: ", newMemberId)

		stmt1, err := tx.Prepare(`SELECT bettype_id, min_amount, max_amount FROM member_limitation WHERE member_id = ?`)
		if err != nil { panic(err) }
		defer stmt1.Close()

		type limitData struct {
			BetTypeId int64
			MinAmount float32
			MaxAmount float32
		}
		var tmpLimitData []limitData
		rows1, err := stmt1.Query(parentId)
		if err != nil { panic(err) }
		for rows1.Next() {
			var data limitData
			err := rows1.Scan(&data.BetTypeId, &data.MinAmount, &data.MaxAmount)
			if err != nil { panic(err) }
			tmpLimitData = append(tmpLimitData, data)
		}
		rows1.Close()
		stmt1.Close()

		for _, row := range tmpLimitData {
			_, err = tx.Exec(
				`INSERT INTO member_limitation (member_id, bettype_id, min_amount, max_amount) VALUES (?, ?, ?, ?)`,
				newMemberId, row.BetTypeId, row.MinAmount, row.MaxAmount)
			if err != nil { panic(err) }
		}

		type tmpStruct struct {
			BetTypeId int64
			Amount float32
		}
		
		stmt2, err := tx.Prepare(`SELECT bettype_id, amount FROM member_premium WHERE member_id = ?`)
		if err != nil { panic(err) }

		var tmpPremium []tmpStruct
		rows2, err := stmt2.Query(parentId)
		if err != nil { panic(err) }
		for rows2.Next() {
			var row tmpStruct
			err := rows2.Scan(&row.BetTypeId, &row.Amount)
			if err != nil { panic(err) }
			tmpPremium = append(tmpPremium, row)
		}
		rows2.Close()
		stmt2.Close()

		for _, row := range tmpPremium {
			_, err = tx.Exec(
				`INSERT INTO member_premium (member_id, bettype_id, amount) VALUES (?, ?, ?)`,
				newMemberId, row.BetTypeId, row.Amount)
			if err != nil { panic(err) }
		}

		stmt3, err := tx.Prepare(`SELECT bettype_id, amount FROM member_proportion WHERE member_id = ?`)
		if err != nil { panic(err) }

		var tmpProportion []tmpStruct
		rows3, err := stmt3.Query(parentId)
		if err != nil { panic(err) }
		for rows3.Next() {
			var row tmpStruct
			err := rows3.Scan(&row.BetTypeId, &row.Amount)
			if err != nil { panic(err) }
			tmpProportion = append(tmpProportion, row)
		}

		for _, row := range tmpProportion {
			_, err = tx.Exec(
				`INSERT INTO member_proportion (member_id, bettype_id, amount) VALUES (?, ?, ?)`,
				newMemberId, row.BetTypeId, row.Amount,
			)
			if err != nil { panic(err) }
		}

		rows3.Close()
		stmt3.Close()
		
		return newMemberId, nil
	})
	return ret.(int64), err
}

func (mr *MemberRepository) GetLimitation (id int64) Collection {
	stmt, err := mr.DB.Prepare(
		`SELECT
			b.id,
			b.name,
			b.display_name,
			ml.min_amount,
			ml.max_amount
		FROM member m
			LEFT JOIN member_limitation ml ON ml.member_id = m.id
			LEFT JOIN bettype b ON b.id = ml.bettype_id
		WHERE m.id = ?`)

	if err != nil {
		panic(err)
	}

	rows, err := stmt.Query(id)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()

	// var limitations []Limitation
	var limitations Collection
	for rows.Next() {
		var l Limitation
		err := rows.Scan(
			&l.BetTypeId,
			&l.BetTypeName,
			&l.BetTypeDisplayName,
			&l.MinAmount,
			&l.MaxAmount,
		)
		if err != nil {
			panic(err)
		}
		limitations.Append(l)
		// limitations.Slice = append(limitations.Slice, l)
	}
	return limitations
}

func (mr *MemberRepository) UpdateLimitation (id int64, collection LimitationCollection) Collection {
	affectedRows := MakeCollection()
	_, err := WithTransaction (mr.DB, func(tx *sql.Tx) (interface{}, error) {
		stmt, err := mr.DB.Prepare(
			`UPDATE member_limitation SET min_amount=?, max_amount=? WHERE member_id=? AND bettype_id=?`)

		if err != nil { panic(err) }

		for _, limitation := range collection.Slice {
			result, err := stmt.Exec(limitation.MinAmount, limitation.MaxAmount, id, limitation.BetTypeId)
			if err != nil { panic(err) }

			affected, _ := result.RowsAffected()
			if affected > 0 {
				affectedRows.Append(limitation)
			}
		}

		return nil, nil
	})

	if err != nil { panic(err) }

	var args []interface{}
	args = append(args, id, affectedRows)
	mr.EventBus.Emit(events.EventPack{
		events.EVENT_LIMITATION_UPDATED,
		args,
	})
	return affectedRows
}

func (mr *MemberRepository) GetPremium (id int64) Collection {
	stmt, err := mr.DB.Prepare(
		`SELECT
			b.id,
			b.name,
			b.display_name,
			amount
		FROM member m
			LEFT JOIN member_premium mp ON mp.member_id = m.id
			LEFT JOIN bettype b ON b.id = mp.bettype_id
		WHERE m.id = ?`)

	if err != nil {
		panic(err)
	}

	rows, err := stmt.Query(id)
	if err != nil {
		panic(err)
	}

	collection := MakeCollection()
	for rows.Next() {
		var p Premium
		err := rows.Scan(
			&p.BetTypeId,
			&p.BetTypeName,
			&p.BetTypeDisplayName,
			&p.Amount,
		)
		if err != nil {
			panic(err)
		}
		collection.Append(p)
	}

	defer stmt.Close()
	defer rows.Close()
	return collection
}

func (mr *MemberRepository) UpdatePremium (id int64, collection PremiumCollection) Collection {
	affectedRows := MakeCollection()
	_, err := WithTransaction (mr.DB, func(tx *sql.Tx) (interface{}, error) {
		stmt, err := mr.DB.Prepare(`UPDATE member_premium SET amount=? WHERE member_id=? AND bettype_id=?`)
		if err != nil { panic(err) }

		for _, item := range collection.Slice {
			result, err := stmt.Exec(item.Amount, id, item.BetTypeId)
			if err != nil { panic(err) }

			affected, _ := result.RowsAffected()
			if affected > 0 {
				affectedRows.Append(item)
			}
		}

		return nil, nil
	})
	if err != nil { panic(err) }

	var args []interface{}
	args = append(args, id, affectedRows)
	mr.EventBus.Emit(events.EventPack{ events.EVENT_PREMIUM_UPDATED, args })
	return affectedRows
}

func (mr *MemberRepository) GetProportion (id int64) Collection {
	stmt, err := mr.DB.Prepare(
		`SELECT
			b.id,
			b.name,
			b.display_name,
			amount
		FROM member m
			LEFT JOIN member_proportion mp ON mp.member_id = m.id
			LEFT JOIN bettype b ON b.id = mp.bettype_id
		WHERE m.id = ?`)

	if err != nil {
		panic(err)
	}

	rows, err := stmt.Query(id)
	if err != nil {
		panic(err)
	}

	collection := MakeCollection()
	// var proportions []Proportion
	for rows.Next() {
		var p Proportion
		err := rows.Scan(
			&p.BetTypeId,
			&p.BetTypeName,
			&p.BetTypeDisplayName,
			&p.Amount,
		)
		if err != nil {
			panic(err)
		}
		// proportions = append(proportions, p)
		collection.Append(p)
	}

	defer stmt.Close()
	defer rows.Close()
	return collection
}

func (mr *MemberRepository) UpdateProportion (id int64, collection ProportionCollection) Collection {
	affectedRows := MakeCollection()
	_, err := WithTransaction (mr.DB, func(tx *sql.Tx) (interface{}, error) {
		stmt, err := mr.DB.Prepare(`UPDATE member_proportion SET amount=? WHERE member_id=? AND bettype_id=?`)
		if err != nil { panic(err) }

		for _, item := range collection.Slice {
			result, err := stmt.Exec(item.Amount, id, item.BetTypeId)
			if err != nil { panic(err) }

			affected, _ := result.RowsAffected()
			if affected > 0 {
				affectedRows.Append(item)
			}
		}

		return nil, nil
	})
	if err != nil { panic(err) }

	var args []interface{}
	args = append(args, id, affectedRows)
	mr.EventBus.Emit(events.EventPack{ events.EVENT_PROPORTION_UPDATED, args })
	return affectedRows
}

func (mr *MemberRepository) GetChildLimitationMaxAmount (id int64, betTypeId int64) float32 {
	row := mr.DB.QueryRow(
		`SELECT SUM(ml.max_amount)
		FROM member m
		LEFT JOIN member_limitation ml ON m.id = ml.member_id
		WHERE m.parent_id = ? AND ml.bettype_id = ?`, id, betTypeId)
	var summary float32
	err := row.Scan(&summary)
	if err != nil { panic(err) }
	return summary
}
