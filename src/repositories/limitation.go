package repositories

type Limitation struct {
	BetTypeId int64 			`json:"betTypeId" binding:"required"`
	BetTypeName string 			`json:"betTypeName"`
	BetTypeDisplayName string 	`json:"betTypeDisplayName"`
	MinAmount float32 			`json:"minAmount" binding:"required"`
	MaxAmount float32 			`json:"maxAmount" binding:"required,min=10,max=10000000,gtfield=MinAmount"`
}

type LimitationCollection struct {
	Slice []Limitation	`json:"data" binding:"required,dive"`
}