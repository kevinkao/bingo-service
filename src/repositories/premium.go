package repositories

type Premium struct {
	BetTypeId int64 			`json:"betTypeId" binding:"required"`
	BetTypeName string 			`json:"betTypeName"`
	BetTypeDisplayName string 	`json:"betTypeDisplayName"`
	Amount float32 				`json:"amount" binding:"required,min=0"`
}

type PremiumCollection struct {
	Slice []Premium	`json:"data" binding:"required,dive"`
}