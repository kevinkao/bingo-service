package repositories

import (
	"fmt"
	"database/sql"
	"repositories/binding"
)

type Member struct {
	Id int64 						`json:"id"`
	LevelId binding.NullInt64 		`json:"levelId"`
	ParentId binding.NullInt64 		`json:"parentId"`
	Name string 					`json:"name"`
	Account string 					`json:"account"`
	LevelName string 				`json:"levelName"`
	LevelSequence int64 			`json:"levelSequence"`
	ParentName binding.NullString 	`json:"parentName"`
	Blocked int64 					`json:"blocked"`

	password string 				`json:"-"`
	memberRepo *MemberRepository	`json:"-"`
}

func (m *Member) SetMemberRepo (memberRepo *MemberRepository) {
	m.memberRepo = memberRepo
}

func (m *Member) SetPassword (password string) {
	m.password = password
}

func (m *Member) GetPassword () string {
	return m.password
}

func (m *Member) SetLevelId (id int64) {
	m.LevelId = binding.NullInt64{
		sql.NullInt64 {
			Int64: id,
			Valid: true,
		},
	}
}

func (m *Member) SetParentId (id int64) {
	m.ParentId = binding.NullInt64{
		sql.NullInt64 {
			Int64: id,
			Valid: true,
		},
	}
}

func (m *Member) SetParentName (name string) {
	m.ParentName = binding.NullString {
		sql.NullString {
			String: name,
			Valid: true,
		},
	}
}

func (m *Member) Store () {
	id, err := m.memberRepo.AddMember(
		m.LevelId.Int64,
		m.ParentId.Int64,
		m.Name,
		m.Account,
		m.password,
	)
	if err != nil { panic(err) }
	defer func () {
		if p := recover(); p != nil {
			fmt.Println(p)
		}
	}()
	row := m.memberRepo.FindById(id)
	m.LevelId = row.LevelId
	m.ParentId = row.ParentId
	m.Name = row.Name
	m.Account = row.Account
	m.LevelName = row.LevelName
	m.LevelSequence = row.LevelSequence
	m.ParentName = row.ParentName
	m.Blocked = row.Blocked
}

// 判斷是否再最頂層
func (m *Member) atTopLevel () bool {
	return m.LevelSequence == LEVEL_OWNER_SEQUENCE
}

// 判斷是否能存取該層級
func (m *Member) CanAccessLevel (level Level) bool {
	// 越小越大
	return m.LevelSequence < level.Sequence
}

// 取得限紅
func (m *Member) GetLimitation () Collection {
	return m.memberRepo.GetLimitation(m.Id)
}

// 取得指定玩法的限紅
func (m *Member) GetBetTypeLimitation(betTypeId int64) Limitation {
	collection := m.GetLimitation()
	matches := collection.Filter(func(i int, item interface{}) bool {
		l := item.(Limitation)
		if l.BetTypeId == betTypeId {
			return true
		}
		return false
	})
	limitation, exists := matches.First()
	if !exists {
		panic("No such limitation")
	}
	return limitation.(Limitation)
}

// 更新限紅
func (m *Member) UpdateLimitation (collection LimitationCollection) Collection {
	return m.memberRepo.UpdateLimitation(m.Id, collection)
}

// 取得退水
func (m *Member) GetPremium () Collection {
	return m.memberRepo.GetPremium(m.Id)
}

// 取得指定玩法退水
func (m *Member) GetBetTypePremium (betTypeId int64) Premium {
	collection := m.GetPremium()
	matches := collection.Filter(func(idx int, item interface{}) bool {
		p := item.(Premium)
		if p.BetTypeId == betTypeId {
			return true
		}
		return false
	})
	premium, exists := matches.First()
	if !exists {
		panic("No such premium")
	}
	return premium.(Premium)
}

// 更新退水
func (m *Member) UpdatePremium (collection PremiumCollection) Collection {
	return m.memberRepo.UpdatePremium(m.Id, collection)
}

// 取得佔成
func (m *Member) GetProportion () Collection {
	return m.memberRepo.GetProportion(m.Id)
}

// 取得指定玩法佔成
func (m *Member) GetBetTypeProportion (betTypeId int64) Proportion {
	collection := m.GetProportion()
	matches := collection.Filter(func(idx int, item interface{}) bool {
		p := item.(Proportion)
		if p.BetTypeId == betTypeId {
			return true
		}
		return false
	})
	proportion, exists := matches.First()
	if !exists {
		panic("No such proportion")
	}
	return proportion.(Proportion)
}

// 更新佔成
func (m *Member) UpdateProportion (collection ProportionCollection) Collection {
	return m.memberRepo.UpdateProportion(m.Id, collection)
}

// 取得上一層的父會員
func (m *Member) GetParent () (Member, bool) {
	var member Member
	if m.ParentId.Valid {
		return m.memberRepo.FindById(m.ParentId.Int64), true
	}
	return member, false
}

// 取得子會員限紅總額加總
func (m *Member) GetChildLimitationMaxAmount (betTypeId int64) float32 {
	return m.memberRepo.GetChildLimitationMaxAmount(m.Id, betTypeId)
}

// 取得所有父會員直到最頂層
func (m *Member) GetParents () []Member {
	var members []Member
	m.memberRepo.GetParents(m.Id, &members)
	return members
}

// 依照給定的層級id來取得在該層級的所有子會員
func (m *Member) GetChildByLevel (level *SessionLevel, page int64, limit int64) []Member {
	if m.LevelSequence == level.Sequence {
		// 搜尋跟自己同一層級, 直接空值回傳
		return make([]Member, 0)
	}

	var parentId []int64
	lastLevel, exists := level.GetLastLevel()
	if !exists || lastLevel.Sequence <= m.LevelSequence {
		// 沒有上一層或者比當前會員還高的層級時, 就回傳會員本身
		parentId = append(parentId, m.Id)
	} else {
		var id []int64
		id = append(id, m.Id)
		parentId = m.memberRepo.SearchMembersLevelByLevel(id, lastLevel.Id)
	}

	var members []Member
	members = m.memberRepo.ListWithinParents(parentId, page, limit)
	return members
}

// 依照帳號搜索(模糊搜索)符合帳號的子會員
func (m *Member) SearchChildByAccount (account string, level *SessionLevel, page int64, limit int64) []Member {
	if m.LevelSequence == level.Sequence {
		// 搜尋跟自己同一層級, 直接空值回傳
		return make([]Member, 0)
	}

	var parentId []int64
	lastLevel, exists := level.GetLastLevel()
	if !exists || lastLevel.Sequence <= m.LevelSequence {
		// 沒有上一層或者比當前會員還高的層級時, 就回傳會員本身
		parentId = append(parentId, m.Id)
	} else {
		var id []int64
		id = append(id, m.Id)
		parentId = m.memberRepo.SearchMembersLevelByLevel(id, lastLevel.Id)
	}

	var members []Member
	members = m.memberRepo.SearchAccountWithinParents(account, parentId, page, limit)
	return members
}
