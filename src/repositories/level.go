package repositories

const (
	LEVEL_OWNER_SEQUENCE    = 5
	LEVEL_OPERATOR_SEQUENCE = 10
	LEVLE_AGENT_SEQUENCE    = 15
	LEVEL_MEMBER_SEQUENCE   = 20
)

type Level struct {
	Id int64
	Name string
	Sequence int64
}

type SessionLevel struct {
	Level
	LevelRepo *LevelRepository
}

func (sl *SessionLevel) GetLastLevel () (Level, bool) {
	var level Level
	if sl.Sequence == LEVEL_OWNER_SEQUENCE {
		return level, false
	}
	levels := sl.LevelRepo.GetAllLevels()
	for i, l := range levels {
		if l.Id == sl.Id {
			level = levels[i - 1]
		}
	}
	return level, true
}