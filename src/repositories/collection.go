package repositories

func MakeCollection () Collection {
	collection := Collection{}
	collection.Slice = make([]interface{}, 0)
	return collection
}

type Collection struct {
	Slice []interface{}
}

func (c *Collection) First() (interface{}, bool) {
	var ret interface{}
	if len(c.Slice) > 0 {
		return c.Slice[0], true
	}
	return ret, false
}

func (c *Collection) Filter (callback func(index int, item interface{}) bool) Collection {
	ret := MakeCollection()
	for i, item := range c.Slice {
		if callback(i, item) {
			ret.Append(item)
		}
	}
	return ret
}

func (c *Collection) Append (item interface{}) {
	c.Slice = append(c.Slice, item)
}

func (c *Collection) AppendSlice (slice interface{}) {
	switch slice.(type) {
	case []int64:
		for _, item := range slice.([]int64) {
			c.Slice = append(c.Slice, item)
		}
	}
}

func (c *Collection) All () []interface{} {
	return c.Slice
}

func (c *Collection) Each (callback func(index int, item interface{}) bool) {
	for idx, item := range c.Slice {
		ret := callback(idx, item)
		if !ret {
			break
		}
	}
}