package repositories

import (
	"database/sql"
	"fmt"
)

func WithTransaction (db *sql.DB, fn func(tx *sql.Tx) (interface{}, error)) (interface{}, error) {
	tx, err := db.Begin()
	if err != nil {
		var ret interface{}
		return ret, err
	}

	defer func() {
		if p := recover(); p != nil {
			fmt.Println(p)
			// a panic occurred, rollback and repanic
			tx.Rollback()
			panic(p)
		} else if err != nil {
			// something went wrong, rollback
			tx.Rollback()
		} else {
			// all good, commit
			err = tx.Commit()
		}
	}()

	ret, err := fn(tx)
	return ret, err
}