package binding

import (
	"encoding/json"
	"database/sql"
	// "reflect"
)

type NullInt64 struct {
	sql.NullInt64
}

func (r NullInt64) MarshalJSON() ([]byte, error) {
	if r.Valid {
		return json.Marshal(r.Int64)
	} else {
		return json.Marshal(nil)
	}
}

func (r NullInt64) UnmarshalJSON(b []byte) error {
	err := json.Unmarshal(b, &r.Int64)
	r.Valid = (err == nil)
	return err
}

type NullString struct {
	sql.NullString
}

func (ns NullString) MarshalJSON() ([]byte, error) {
	if !ns.Valid {
		return []byte("null"), nil
	}
	return json.Marshal(ns.String)
}

func (ns NullString) UnmarshalJSON(b []byte) error {
	err := json.Unmarshal(b, &ns.String)
	ns.Valid = (err == nil)
	return err
}