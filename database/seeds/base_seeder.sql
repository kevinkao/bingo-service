INSERT INTO level (`id`, `name`, `sequence`) VALUES
(1, '公司', 5), (2, '階層1', 10), (3, '階層2', 15), (4, '會員', 20);

INSERT INTO bettype (`id`, `name`, `display_name`, `odds`) VALUES
(1, 'basic', '基本玩法', 0), (2, 'super', '超級獎號', 0), (3, 'over_under', '猜大小', 0), (4, 'odd_even', '猜單雙', 0);